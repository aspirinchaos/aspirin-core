import { Mongo } from 'meteor/mongo';
import { newError, throwError } from './error';
import './extend/string';
import './extend/number';
import './extend/math';
// тип для сообщений
import MessageBox from 'message-box';
// дефолтные сообщения ru
import messageBox from './messagebox';
import { check, Match } from 'meteor/check';
import getFormData from './get-form-data';
import CommonSchema from './common-schema';
import moment from 'moment/moment';
import Popup from './sweetalert';

const DATE = 'DD.MM.YYYY';
const TIME = 'HH:mm';
const own = Object.prototype.hasOwnProperty;
/**
 * BS inspinia цвета
 * @type {{primary, default, success, info, warning, danger}}
 */
const Colors = {
  primary: '#1ab394',
  default: '#c2c2c2',
  success: '#1c84c6',
  info: '#23c6c8',
  warning: '#f8ac59',
  danger: '#ED5565',
};

/**
 * Общий класс для наследования всех экземпляров классов
 */
class Collection {
  constructor(doc) {
    Object.assign(this, doc);
  }

  accessInner(name) {
    if (Array.isArray(name)) {
      return name.map(item => this.accessInner(item));
    }
    // длина всегда будет будет 1 или больше
    const parts = name.split('.');

    if (own.apply(name, this) || parts.length === 1) {
      return this[name];
    }
    // выполнится вложенность если длина 2 и больше
    let value = this;
    parts.forEach((part) => {
      value = value[part] || '';
    });
    return value;
  }

  checkField(field) {
    if (!this[field]) {
      throwError('bad-value', `В документе нет поля ${field}`);
    }
  }

  get dateFormat() {
    return DATE;
  }

  get createdDate() {
    this.checkField('createdAt');
    return moment(this.createdAt).format(this.dateFormat);
  }

  get updatedDate() {
    this.checkField('updatedAt');
    return moment(this.updatedAt).format(this.dateFormat);
  }

  get timeFormat() {
    return TIME;
  }

  get createdTime() {
    this.checkField('createdAt');
    return moment(this.createdAt).format(this.timeFormat);
  }

  get updatedTime() {
    this.checkField('updatedAt');
    return moment(this.updatedAt).format(this.timeFormat);
  }

  get dateTimeFormat() {
    return `${this.dateFormat} ${this.timeFormat}`;
  }

  get createdDateTime() {
    this.checkField('createdAt');
    return moment(this.createdAt).format(this.dateTimeFormat);
  }

  get updatedDateTime() {
    this.checkField('updatedAt');
    return moment(this.updatedAt).format(this.dateTimeFormat);
  }

  static checkQuery(query) {
    if (!query) {
      return {};
    }
    if (Match.test(query, Object)) {
      return query;
    }
    return { _id: query };
  }
}

class MongoCollection extends Mongo.Collection {
  constructor(name, CollectionClass) {
    super(name, {
      transform(doc) {
        return new CollectionClass(doc);
      },
    });
    this._transformClass = CollectionClass;
  }

  /**
   * Класс который используется для документа коллекции
   * @returns {Collection}
   */
  get DocClass() {
    return this._transformClass;
  }

  /**
   * Функция для получения опций для селекта
   * @param name - имя значения
   * @param key - имя ключа
   */
  findOptions(name, key = '_id') {
    return this.find().map(({ [key]: value, [name]: label }) => ({ value, label }));
  }
}

/**
 * Функция для создания коллекций
 * @param name {String} - имя коллекции
 * @param CollectionClass {function} - класс экземпляра коллекции
 * @param schema {SimpleSchema} - схема для коллекции
 * @param msgbox {MessageBox} - схема для коллекции
 * @returns {Mongo.Collection}
 */
const newCollection = (name, CollectionClass, schema = null, msgbox = null) => {
  // создадим саму коллекцию
  const col = new MongoCollection(name, CollectionClass);
  // привяжем схему
  if (schema) {
    if (msgbox && Match.test(msgbox, MessageBox)) {
      schema.messageBox = msgbox;
    } else {
      schema.messageBox = messageBox;
    }
    col.attachSchema(schema);
  }

  return col;
};

export {
  Collection,
  MongoCollection,
  newCollection,
  newError,
  throwError,
  getFormData,
  messageBox,
  DATE,
  TIME,
  CommonSchema,
  Popup,
  Colors,
};
