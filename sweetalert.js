import Swal from 'sweetalert2';
import { Match } from 'meteor/check';

const confirmDefault = {
  text: 'Ок',
  color: '#3085d6',
};

const cancelDefault = {
  text: 'Отмена',
  color: '#aaa',
};

const swalConfirm = ({
  title = '', type = 'warning', text = '', confirm = confirmDefault, cancel = cancelDefault, ...other
}, cb = () => {}) => {
  if (Match.test(confirm, String)) {
    confirm = { ...confirmDefault, text: confirm };
  }
  if (Match.test(cancel, String)) {
    cancel = { ...cancelDefault, text: cancel };
  }
  Swal({
    title,
    text,
    type,
    showCancelButton: true,
    confirmButtonText: confirm && confirm.text,
    confirmButtonColor: confirm && confirm.color,
    cancelButtonText: cancel && cancel.text,
    cancelButtonColor: cancel && cancel.color,
    ...other,
  }).then(cb);
};

const swalSuccess = ({ title = '', text = '', ...other }, cb = () => {}) => {
  Swal({
    title,
    text,
    type: 'success',
    ...other,
  }).then(cb);
};

const swalError = ({ title = '', text = '', ...other }, cb = () => {}) => {
  Swal({
    title,
    text,
    type: 'error',
    ...other,
  }).then(cb);
};

export {
  Swal,
  swalConfirm as confirm,
  swalSuccess as success,
  swalError as error,
};
