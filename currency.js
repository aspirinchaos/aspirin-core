function part(n, parts) {
  const m = n % 10;
  const x = n / 10;
  // исключаем 11..19
  if (x < 1 || x > 2) {
    if (m === 1) {
      return parts[0];
    }
    if (m > 1 && m < 5) {
      return parts[1];
    }
  }
  return parts[2];
}

function get_currency(number, c = 'rub') {
  const curArr = {
    rub: ['рубль', 'рубля', 'рублей'],
  };

  return part(number, curArr[c]);
}

function number_to_string(_number, currency) {
  const _arr_numbers = [];
  _arr_numbers[1] = ['',
    'один',
    'два',
    'три',
    'четыре',
    'пять',
    'шесть',
    'семь',
    'восемь',
    'девять',
    'десять',
    'одиннадцать',
    'двенадцать',
    'тринадцать',
    'четырнадцать',
    'пятнадцать',
    'шестнадцать',
    'семнадцать',
    'восемнадцать',
    'девятнадцать',
  ];
  _arr_numbers[2] = ['', '', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
  _arr_numbers[3] = ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];
  function number_parser(_num, _desc) {
    let _string = '';
    let _num_hundred = '';
    if (_num.length === 3) {
      _num_hundred = _num.substr(0, 1);
      _num = _num.substr(1, 3);
      _string = `${_arr_numbers[3][_num_hundred]} `;
    }
    if (_num < 20) {
      _string += `${_arr_numbers[1][parseFloat(_num)]} `;
    } else {
      const _first_num = _num.substr(0, 1);
      const _second_num = _num.substr(1, 2);
      _string += `${_arr_numbers[2][_first_num]} ${_arr_numbers[1][_second_num]} `;
    }

    switch (_desc) {
      case 0:
        _string += get_currency(_num, currency);
        break;
      case 1:
        _string += part(_num, ['тысяча ', 'тысячи ', 'тысяч ']);
        _string = _string.replace('один ', 'одна ');
        _string = _string.replace('два ', 'две ');
        break;
      case 2:
        _string += part(_num, ['миллион ', 'миллиона ', 'миллионов ']);
        break;
      case 3:
        _string += part(_num, ['миллиард ', 'миллиарда ', 'миллиардов ']);
        break;
    }
    _string = _string.replace('  ', ' ');
    return _string;
  }

  function decimals_parser(_num) {
    const _first_num = _num.substr(0, 1);
    const _second_num = parseFloat(_num.substr(1, 2));
    let _string = ` ${_first_num}${_second_num}`;

    _string += part(_num, [' копейка', ' копейки', ' копеек']);
    return _string;
  }

  if (_number === 0) {
    return false;
  }

  _number = _number.toFixed(2);

  const _number_arr = _number.split('.');

  _number = _number_arr[0];

  const _number_decimals = _number_arr[1];
  const _number_length = _number.length;
  let _string = '';
  let _num_parser = '';
  let _count = 0;

  for (let _p = (_number_length - 1); _p >= 0; _p--) {
    _num_parser = _number.substr(_p, 1) + _num_parser;

    if ((_num_parser.length === 3 || _p === 0) && !isNaN(parseFloat(_num_parser))) {
      _string = number_parser(_num_parser, _count) + _string;
      _num_parser = '';
      _count++;
    }
  }
  if (_number_decimals) _string += decimals_parser(_number_decimals);
  return _string;
}

export default function (currency = 'rub') {
  return number_to_string(this, currency).capitalize();
}
