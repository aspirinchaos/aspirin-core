/* eslint-disable no-extend-native */

import moneyString from '../currency';

Number.prototype.moneyToString = moneyString;

Number.prototype.roundToFixed = function(f = 1) {
    // this является примитивой (объектом). при работе с ней this превращается в число
    return (Math.round(this) !== this.valueOf()) ? this.toFixed(f) : this;
};

Number.prototype.money = function () {
  return this.toFixed(2);
};
