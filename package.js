Package.describe({
  name: 'aspirin-core',
  version: '0.4.7',
  summary: 'core functions and api',
  documentation: 'README.md',
});

Npm.depends({
  'message-box': '0.2.0',
  moment: '2.21.0',
  sweetalert2: '7.19.1',
});

Package.onUse(function (api) {
  api.versionsFrom('1.5.1');
  api.use(['ecmascript', 'mongo', 'aldeed:collection2@3.0.0']);
  api.mainModule('core.js');
});
