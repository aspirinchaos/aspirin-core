import { Meteor } from 'meteor/meteor';

class Error extends Meteor.Error {
  constructor(error, reason, details, data) {
    super(error, reason, details);
        // todo@aspirin добавить логер ошибок
  }
}

/**
 * @param {String} error
 * @param {String} [reason]
 * @param {String} [details]
 * @param data
 * @returns {Error}
 */
const newError = function (error, reason, details, ...data) {
  return new Error(error, reason, details, data);
};
const throwError = function (error, reason, details, ...data) {
  throw newError(error, reason, details, data);
};

export {
  newError,
  throwError,
};
