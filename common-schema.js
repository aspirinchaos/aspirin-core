import SimpleSchema from 'simpl-schema';
import messageBox from './messagebox';

const CommonSchema = new SimpleSchema({
  createdAt: {
    type: Date,
    label: 'Дата создания',
    autoValue() {
      if (this.isInsert && !this.isSet) {
        return new Date();
      }
    },
  },
  updatedAt: {
    type: Date,
    label: 'Дата обновления',
    autoValue: () => new Date(),
  },
});

CommonSchema.messageBox = messageBox;

export default CommonSchema;
